#include <cstring>
#include <cstdlib>
#include <ctime>
#include "main.hpp"

/* Items class members */

Items::Items(void)
{
	strcpy(description, "This is a unidentified item");
}

void Items::set_weight(float value)
{
	weight = value;
}

char *Items::get_description(void)
{
	return description;
}

float Items::get_weight(void)
{
	return weight;
}

/* Weapon class members */

Weapon::Weapon(void)
{
	is_magic = 0;
	srand(time(NULL));
	if (is_magic)
		durability = 50;
	else
		durability = rand() % 50 + 1;
}

int Weapon::attack(void)
{
	srand(time(NULL));
	return rand() % dice_type + dices;	
}

int Weapon::get_durability(void)
{
	return durability;
}

/* Dagger class members */

Dagger::Dagger(void)
{
	dices = 1;
	dice_type = 4;
	weight = 0.4;
	strcpy(description, "This is a dagger");
}
