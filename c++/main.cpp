#include <iostream>
#include "main.hpp"

int
main(void)
{
	Items generic;
	Dagger dagger;
	generic.set_weight(1.2);
	std::cout << generic.get_weight() << '\n';
	std::cout << generic.get_description() << '\n';
	std::cout << "dagger attack = " << dagger.attack() << '\n';
	std::cout << "dagger weight = " << dagger.get_weight() << '\n';
	std::cout << "dagger durability = " << dagger.get_durability() << '\n';
	std::cout << "dagger description = " << dagger.get_description() << '\n';
	return 0;
}
