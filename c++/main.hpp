#ifndef MAIN_H
#define MAIN_H

class Items {
	protected:
		float weight;
		char description[1000];
	public:
		Items(void);
		void set_weight(float weight);
		char *get_description(void);
		float get_weight(void);
};

class Weapon: public Items {
	protected:
		int is_magic;
		int dices;
		int dice_type;
		int durability;
	public:
		Weapon(void);
		int attack(void);
		int get_durability(void);
};

class Dagger: public Weapon {
	private:
		int usable_class;
	public:
		Dagger();
};

#endif
