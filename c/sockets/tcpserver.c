#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>

struct sockaddr_in server;

void usage(void)
{
	puts("Usage: portnumber");
}

int main(int argc, char **argv)
{
	char buffer[100];
	int size, sock, status, port;

	size = sizeof(server);

	if (argc != 2) {
		fputs("Incorrect number of arguments!\n", stderr);
		usage();
		exit(1);
	}
	/* convert port value from int to string */
	port = strtol(argv[1], NULL, 10);

	/* create socket */
	sock = socket(AF_INET, SOCK_STREAM, 0);
	if (!sock) {
		perror("socket");
		exit(1);
	}

	/* set client's data structure */
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(port);

	/* attach socket to specified port */
	status = bind(sock, (struct sockaddr*) &server, size);
	if (status == -1) {
		perror("bind");
		exit(1);
	}

	/* listen the port */
	status = listen(sock, 3);
	if (status < 0) {
		perror("listen");
		exit(1);
	}
	printf("server online!\n");

	/* accept the connection and check if the connection was succesfully accepted */
	sock = accept(sock,(struct sockaddr*) &server, &size);
	if (sock < 0) {
		perror("accept");
		exit(1);
	}
	
	read(sock, buffer, sizeof(buffer));
	puts(buffer);
	return 0;
}
