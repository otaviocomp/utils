#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>

#define porta 6666

int main()
{
	int number;
	struct sockaddr_in servidor;
	int servidorsize = sizeof(servidor);
	int sock = socket(AF_INET, SOCK_DGRAM, 0);

	// check if the socket was successfully created
	if (sock == -1) {
		perror("socket ");
		exit(1);
	}

	// configurations
	servidor.sin_family = AF_INET;
	servidor.sin_port = htons(porta);
	servidor.sin_addr.s_addr = htonl(INADDR_ANY);
	memset(servidor.sin_zero, 0, 8);

	int res_bind = bind(sock, (struct sockaddr*) &servidor,
	                       sizeof(servidor));
	if (res_bind == -1) {
		perror("bind ");
		exit(1);
	}

	int res_recvfrom;	
	while(1) {
		res_recvfrom = recvfrom(sock, &number, sizeof(number), 
		                        0, (struct sockaddr*) &servidor, 
		                        &servidorsize);
		if ( res_recvfrom > 0)
			printf("%d\n", number);
	}
}
