#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

struct sockaddr_in client;

void usage(void)
{
	puts("Usage: server_ip portnumber");
}

int main(int argc, char **argv)
{
	const char message[] = "hello world from client!";
	int server, status, port;

	if (argc != 3) {
		fputs("Incorrect number of arguments!\n", stderr);
		usage();
		exit(1);
	}
	/* convert port value from int to string */
	port = strtol(argv[2], NULL, 10);

	/* create socket */
	int sock = socket(AF_INET, SOCK_STREAM, 0);
	
	/* check if the socket was successfully created */
	if (sock == -1) {
		perror("socket ");
		exit(1);
	}

	/* set client's data structure */
	client.sin_family = AF_INET;
	client.sin_addr.s_addr = inet_addr(argv[1]);
	client.sin_port = htons(port);

	/* connect to server */
	status = connect(sock,(struct sockaddr*) &client, sizeof(client)); 
	if (status == -1) {
		perror("connect ");
		exit(1);
	}

	send(sock, message, sizeof(message), 0);
}
