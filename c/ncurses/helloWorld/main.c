#include <ncurses.h>

int main()
{
	initscr();                /* initialize curses mode */
	printw("Hello World!");   /* print text */
	refresh();                /* print to real screen */
	getch();                  /* user input */
	endwin();                 /* end curses mode */

	return 0;
}
