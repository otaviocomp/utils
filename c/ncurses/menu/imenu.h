#ifndef H_MENU
#define H_MENU

#include <ncurses.h>
#include <stdbool.h>

/* global variables */
int row;
int col;

static const char *options[] = {
	"Option1", "Option2", "Help", "Exit"
};

static const char *sel_opt[] = {
	"Option 1 was selected!",
	"Option 2 was selected!",
	"Help menu selected! use 'j' and 'k' to move"
};

#endif
