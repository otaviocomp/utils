#include "imenu.h"

void    init(void);
int     control(int, int, int, int, int, WINDOW *);
void    dialogbox(int, int, int, int, const char *);
int     option_selected(int, int, int, int, int);
WINDOW *create_newwin(int, int, int, int);

int
main(int argc, char **argv)
{
	int size;
	int opt;
	int quit;
	int height;
	int width;
	int starty;
	int startx;
	WINDOW *menu;

	/* init ncurses and other functions */
	init();

	/* get size of the list of options */
	size = sizeof(options) / sizeof(options[0]);

	/* get lines and columns of stdscr */
	getmaxyx(stdscr, row, col);

	/* window specifications */
	height = size + 2;
	width  = 40;
	starty = (LINES - height) / 2;
	startx = (COLS - width) / 2;
	quit   = 0;


	while (!quit) {
		/* create window */
		menu = create_newwin(height, width, starty, startx);

		wattron(menu, A_BOLD);
		opt = control(height, width, starty, startx, size, menu);
		wattroff(menu, A_BOLD);

		delwin(menu);

		quit = option_selected(height, width, starty, startx, opt);
	}

	/* end ncurses mode */
	endwin();

	return 0;
}

void
init(void)
{
	initscr();
	raw();
	noecho();
	curs_set(0);
	keypad(stdscr, TRUE);

	start_color();

	/* init color pairs */
	init_pair(1, COLOR_RED, COLOR_BLACK);
	init_pair(2, COLOR_GREEN, COLOR_BLACK);
	init_pair(3, COLOR_YELLOW, COLOR_BLACK);
	init_pair(4, COLOR_BLUE, COLOR_BLACK);
	init_pair(5, COLOR_MAGENTA, COLOR_BLACK);
	init_pair(6, COLOR_CYAN, COLOR_BLACK);
	init_pair(7, COLOR_WHITE, COLOR_BLACK);
}

int
control(int height, int width, int starty, int startx, int size, WINDOW *win)
{
	int i;
	bool selected;
	static int sel;

	selected = false;
	while (!selected) {
		for (i = 0; i < size; i++) {
			if (i == sel)
				/* highlight selected option */
				wattron(win, COLOR_PAIR(COLOR_RED));
			else
				wattroff(win, COLOR_PAIR(COLOR_RED));
			/* list options */
			mvwprintw(win, i+1, 1, options[i]);
		}
		wrefresh(win);

		switch (wgetch(win)) {
			case 'k':
				sel--;
				sel = (sel < 0) ? size - 1 : sel;
				break;
			case 'j':
				sel++;
				sel = (sel > size - 1) ? 0 : sel;
				break;
			case 10:
				selected = true;
				break;
		}
	}

	return sel;
}

WINDOW *
create_newwin(int height, int width, int starty, int startx)
{
	WINDOW *win;
	win = newwin(height, width, starty, startx);
	box(win, 0, 0);

	wrefresh(win);

	return win;
}

void
dialogbox(int height, int width, int starty, int startx, const char *text)
{
	WINDOW *win;
	win = create_newwin(height, width, starty, startx);

	wattron(win, A_BOLD);
	mvwprintw(win, 1, 1, text);
	wattroff(win, A_BOLD);

	wrefresh(win);

	wgetch(win);
	delwin(win);

	return;
}

int
option_selected(int height, int width, int starty, int startx, int opt)
{
	int quit;
	enum{OPTION1, OPTION2, HELP, EXIT};

	quit = 0;
	switch (opt) {
		case OPTION1:
			dialogbox(height, width, starty, startx, sel_opt[0]);
			break;
		case OPTION2:
			dialogbox(height, width, starty, startx, sel_opt[1]);
			break;
		case HELP:
			dialogbox(height, width, starty, startx, sel_opt[2]);
			break;
		case EXIT:
			quit = 1;
			break;
	}
	return quit;
}

