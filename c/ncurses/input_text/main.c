#include <ncurses.h>
#include <string.h>

int main()
{
	char mesg[] = "Hello, type a text: ";
	char str[80];
	int row, col;

	/* init curses mode */
	initscr();
	cbreak();
	/* get the number of rows and columns */
	getmaxyx(stdscr, row, col);

	/* print message at the center */
	mvprintw(row/2, (col - strlen(mesg))/2, "%s", mesg);

	attron(A_BOLD);
	/* print string at inferior left corner */
	mvprintw(row - 2, 0, "This screen has %d rows and %d cols\n", row, col);
	attroff(A_BOLD);

	printw("Enter text: ");
	getstr(str);
	mvprintw(row/2 + 2, (col - strlen(mesg))/2, "You entered: %s\n", str);

	refresh();
	getch();
	endwin();

	return 0;
}
