#include <ncurses.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
	/* init curses mode */
	initscr();
	start_color();

	/* init color pairs */
	init_pair(1, COLOR_GREEN, COLOR_BLACK);
	init_pair(2, COLOR_RED, COLOR_BLACK);

	/* text */
	mvprintw(0, 0, "Just a random string");
	mvchgat(0, 0, -1, A_BLINK, 1, NULL);
	mvprintw(1, 0, "Another string");
	mvchgat(1, 0, -1, A_NORMAL, 2, NULL);

	refresh();
	getch();
	endwin();

	return 0;
}
