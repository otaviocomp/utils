#include <ncurses.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
	int ch, prev;
	int goto_prev, y, x;
	FILE *fp;

	goto_prev = FALSE;

	if (argc != 2) {
		fputs("Usage: %s <a c file name>\n", stderr);
		exit(1);
	}
	fp = fopen(argv[1], "r");
	if (!fp) {
		perror("Cannot open input file");
		exit(1);
	}

	/* init curses mode */
	initscr();
	cbreak();

	prev = EOF;
	while ((ch = fgetc(fp)) != EOF) {
		if (prev == '/' && ch == '*') {
			attron(A_BOLD);
			goto_prev = TRUE;
		}
		if (goto_prev == TRUE) {
			getyx(stdscr, y, x);
			move(y, x - 1);
			printw("%c%c", '/', ch);
			ch = 'a';
			goto_prev = FALSE;
		}
		else
			printw("%c", ch);
		refresh();
		if (prev == '*' && ch == '/')
			attroff(A_BOLD);
		prev = ch;
	}
	getch();
	endwin();

	return 0;
}
