#include <stdio.h>
#include <getopt.h>

int
main(int argc, char **argv)
{
	int opt;
	while ((opt = getopt(argc, argv, "abcf:")) != -1) {
		switch (opt) {
			case 'a':
			case 'b':
			case 'c':
				printf("option: %c\n", opt);
				break;
			case 'f':
				printf("filename: %s\n", optarg);
				break;
			case ':':
				printf("option needs a value\n");
				break;
			case '?':
				printf("unknown option: %c\n", optopt);
				break;
		}
	}

	for (; optind < argc; optind++)
		printf("extra arguments: %s\n", argv[optind]);

	return 0;
}
