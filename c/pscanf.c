#include <stdio.h>
#include <stdarg.h>

int
pscanf(const char *path, const char *fmt, ...)
{
	int ret;
	FILE *fp;
	va_list ap;

	fp = fopen(path, "r");
	if (!fp) {
		fprintf(stderr, "Cannot open file %s\n", path);
		return -1;
	}

	va_start(ap, fmt);
	ret = vfscanf(fp, fmt, ap);
	va_end(ap);
	fclose(fp);

	return (ret == EOF) ? -1 : ret;
}

int
main(int argc, char *argv[])
{
	int temp, status;

	status = pscanf("/ss/class/thermal/thermal_zone0/temp", "%u", &temp);
	if (status == -1)
		puts("failed to get temperature value");
	else
		printf("%d\n", temp);
	return 0;
}
