#include <stdio.h>
#include <stdarg.h>

double average(int num, ...)
{
	va_list ap;
	double sum = 0.0;
	int i;

	/* initialize memory reserved for ap */
	va_start(ap, num);

	/* access all the arguments assigned to ap */
	for (i = 0; i < num; i++)
		sum += va_arg(ap, int);

	/* clean memory reserved for ap */
	va_end(ap);

	return sum/num;
}


int main(int argc, char **argv)
{
	printf("Average of 2, 3, 4, 5 = %f\n", average(4, 2, 3, 4, 5));
	printf("Average of 5, 10, 15 = %f\n", average(3, 5, 10, 15));
}
