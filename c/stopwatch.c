#include <stdio.h>
#include <unistd.h>

int
main(void)
{
	int hours, minutes, seconds;

	hours = minutes = seconds = 0;

	while (1) {
		printf("\r%02d:%02d:%02d", hours, minutes, seconds++);
		if (seconds == 60) {
			seconds = 0;
			minutes++;
		}
		if (minutes == 60) {
			minutes = 0;
			hours++;
		}
		fflush(stdout);
		sleep(1);
	}
	return 0;
}
