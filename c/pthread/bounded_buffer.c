#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <semaphore.h>

#include "aes.h"

#define BUFFER_SIZE 20

/* function prototypes */

void *producer(void *);
void *consumer(void *);
void  usage(void);
void  print(const char *, int);
void  insert_item(void);
void  remove_item(void);

/* global variables */

int             index_producer;
int             index_consumer;
int             buffer[BUFFER_SIZE];
pthread_mutex_t mutex;
sem_t           full;
sem_t           empty;

/******************/
/* BEGIN OF LOGIC */
/******************/

int
main (int argc, char *argv[])
{
	int n_producer, n_consumer, producer_sleep, consumer_sleep;
	int i;
	pthread_t pid, cid;

	/* check number of arguments */
	if (argc != 3)
		usage();
	
	/* get values and set index */
	n_producer     = atoi(argv[1]);
	n_consumer     = atoi(argv[2]);
	index_producer = index_consumer = 0;

	/* initialize semaphores */
	sem_init(&full, 0, 0);
	sem_init(&empty, 0, BUFFER_SIZE);

	/* fill buffer with zeros */
	for (i = 0; i < BUFFER_SIZE; i++)
		buffer[i] = 0;

	/* create producers and consumers */
	for (i = 0; i < n_producer; i++)
		if (pthread_create(&pid, NULL, producer, &producer_sleep)) {
			fputs("Cannot initialize productor thread\n", stderr);
			exit(1);
		}
	for (i = 0; i < n_consumer; i++)
		if (pthread_create(&cid, NULL, consumer, &consumer_sleep)) {
			fputs("Cannot initialize consumer thread\n", stderr);
			exit(1);
		}

	/* wait for all the threads to finish */
	for (i = 0; i < n_producer; i++)
		pthread_join(pid, NULL);
	for (i = 0; i < n_producer; i++)
		pthread_join(cid, NULL);

	sem_destroy(&full);
	sem_destroy(&empty);

	return 0;
}

/*********************************/
/* PRODUCER AND CONSUMER THREADS */
/*********************************/

void *
producer(void *arg)
{
	srand(time(NULL));

	while (1) {
		sleep((rand() % 5) + 1);

		sem_wait(&empty);
		pthread_mutex_lock(&mutex);

		insert_item();

		pthread_mutex_unlock(&mutex);
		sem_post(&full);
	}
}

void *
consumer(void *arg)
{
	srand(time(NULL));

	while (1) {
		sleep((rand() % 5) + 1);

		sem_wait(&full);
		pthread_mutex_lock(&mutex);

		remove_item();

		pthread_mutex_unlock(&mutex);
		sem_post(&empty);
	}
}

/*******************************************************/
/* INSERT AND REMOVE ROTINES (INSIDE CRITICAL SECTION) */
/*******************************************************/

void
insert_item(void)
{
	srand(time(NULL));

	buffer[index_producer] = rand() % 100;
	print(GREEN, index_producer);
	index_producer = (index_producer + 1) % BUFFER_SIZE;
}

void
remove_item(void)
{
	buffer[index_consumer] = 0;
	print(RED, index_consumer);
	index_consumer = (index_consumer + 1) % BUFFER_SIZE;
}

/*****************/
/* MISCELLANEOUS */
/*****************/

void
usage(void)
{
	puts("Usage: <number_of_producers> <number_of_consumers>");
	puts("Example: ./bounded_buffer 4 3 2 3");
	exit(1);
}

void
print(const char *color, int index) {
	int i;

	printf("%s" " %lu produced %d in buffer[%d]\n" RESET, color, pthread_self(),
	       buffer[index], index);

	printf("%s" "[", color);
	for (i = 0; i < BUFFER_SIZE - 1; i++)
		if (i == index)
			printf(YELLOW "%2d, " "%s", buffer[i], color);
		else
			printf("%s" "%2d, ", color, buffer[i]);
	if (index == BUFFER_SIZE - 1)
		printf(YELLOW "%2d" "%s" "]\n", buffer[BUFFER_SIZE - 1], color);
	else
		printf("%2d]\n" RESET, buffer[BUFFER_SIZE - 1]);
}
