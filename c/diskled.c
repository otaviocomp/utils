#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define FILELED "/sys/class/leds/input3::capslock/brightness"
#define FILEDISK "/proc/diskstats"

int main(void)
{
	pid_t pid;
	char read[120];
	char oldread[120];
	unsigned int usecs = 80000;

	FILE *fled;
	FILE *fdisk;

	pid = fork();
	if (pid < 0) {
		fputs("An error occurred\n", stderr);
		exit(EXIT_FAILURE);
	}

	/* kill parent process */
	if (pid > 0)
		exit(EXIT_SUCCESS);

	/* daemonize */
	if (setsid() < 0)
		exit(1);

	/* main loop */
	while (1) {
		/* first read */
		fdisk = fopen(FILEDISK, "r");
		if (fdisk == NULL) {
			fputs("file cannot be opened\n", stderr);
			exit(EXIT_FAILURE);
		}

		/* get content of first read */
		if ((fgets(oldread, 120, fdisk)) == NULL) {
			fputs("cannot get file content\n", stderr);
			exit(EXIT_FAILURE);
		}
		fclose(fdisk);

		/* interval between reading */
		usleep(usecs);

		/* second read */
		fdisk = fopen(FILEDISK, "r");
		if (fdisk == NULL) {
			fputs("file cannot be opened\n", stderr);
			exit(EXIT_FAILURE);
		}

		/* get content of second read */
		if ((fgets(read, 120, fdisk)) == NULL) {
			fputs("cannot get file content\n", stderr);
			exit(EXIT_FAILURE);
		}
		fclose(fdisk);

		/* open FILELED file */
		fled = fopen(FILELED, "w");
		if (fled == NULL) {
			fputs("file cannot be opened\n", stderr);
			exit(EXIT_FAILURE);
		}

		/* compare the values */
		if (strcmp(read, oldread))
			fprintf(fled, "1");
		else
			fprintf(fled, "0");
		fclose(fled);
	}

	return 0;
}
