#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define DEFAULT_DICE 20
#define DEFAULT_QUANTITY 1
#define DEFAULT_DICE_ONE_ARGUMENT 6

void  usage(void);
void  print(int, int, int*);
int  *dice(int, int);

int
main(int argc, char **argv)
{
	int dices, dice_type;
	int *result;

	switch(argc) {
		case 1:
			/* execute the program with no arguments */
			dice_type = DEFAULT_DICE;
			dices = DEFAULT_QUANTITY;
			result = dice(dices, dice_type);
			print(DEFAULT_QUANTITY, DEFAULT_DICE, result);
			break;
		case 2:
			dice_type = DEFAULT_DICE_ONE_ARGUMENT;

			dices = atoi(argv[1]);
			if (!dices) {
				fputs("Invalid! Must be a number\n", stderr);
				usage();
			}

			result = dice(dices, dice_type);
			print(dices, dice_type, result);
			break;
		case 3:
			dice_type = atoi(argv[2]);

			dices = atoi(argv[1]);
			if (!dices) {
				fputs("Invalid! Must be a number\n", stderr);
				usage();
			}

			result = dice(dices, dice_type);
			print(dices, dice_type, result);
			break;
		default:
			usage();
			break;
	}

	free(result);

	return 0;
}

void
usage(void)
{
	puts("\nroll <quantity>d<number of sides>");
	puts("Example: roll 2 20");
	puts("roll two dices of twenty sides");
	exit(1);
}

void
print(int quantity, int type, int *result)
{
	int i, total;

	total = 0;

	printf("%dd%d\n\n", quantity, type);
	for (i = 0; i < quantity; i++) {
		printf("1d%d = %d\n", type, result[i]);
		total += result[i];
	}
	printf("\nTotal = %d\n", total);
}

int *
dice(int quantity, int type)
{
	int  i;
	int *ret;

	ret = malloc(quantity * sizeof(int));

	srand(time(NULL));

	for (i = 0; i < quantity; i++)
		ret[i] = rand() % type + 1;
	return ret;
}
