\" this is a comment
.TL
This is a title
.AU
Author

.NH
Section 1
.PP
This is a paragraph with a
.B "bold text"
and an
.I "italic text"
and that is all, for now.

.NH
This is another section
.NH 2
Now, this is a subsection
.NH 3
How about a subsubsection?

.NH
Images
.PP
Use images require convert from a traditional format like .png or .jpeg to
postscript format .eps. This can be made using the "convert" program from
ImageMagick package.
.PP
 Example: convert image.png image.eps

\" This is an image
.PSPIC -C "images/image.eps"

\" This is a table
.TS
tab(:) center;
|c s s|
|c s|c|
|c|c|c|.
_
title of the table
_
Title 1: Title 2
_
column 1:column 2:column 3
_
name1:name2:1234
_
.TE

.NH
Some equation examples

.EQ
Delta = b sup 2 - 4ac
.EN

.EQ
int from 0 to 13 x sup 2 dx
.EN

.EQ
lim from {x -> inf} f(x) over x
.EN

.EQ
sum from 0 to inf 1 over x
.EN

.EQ
y >= 5
.EN

.NH
Now, some itens

.IP \(bu
This is an item
.IP \(bu
This is another item
.IP \(bu
Last item
